# VSN Searcher

An application for searching through a db of VSN Sequences

### Getting started

1. Clone this repo

2. Install all the necessary packages (best done inside of a [virtual environment](http://docs.python-guide.org/en/latest/dev/virtualenvs/))
> pip install -r requirements.txt

3. Run the app
> python runserver.py

4. Create and seed the db (the server must still be running, so open a new terminal window first)
> python manage.py create_db && python manage.py seed_db --seedfile 'data/vsn_data.csv'

5. Go home to the search page
> http://localhost:5000/

6. Search for a VSN Sequence
> e.g. ZJTEAV000000



### Testing

#####Python
Testing is done using the unittest framework. From the root folder run (in the virtualenvironment)
> python -m unittest discover

This will automatically find and run all Tests in the application

#####Javascript
For testing, you must have [Node.js](https://nodejs.org/) and Google Chrome installed. We use the [Karma](http://karma-runner.github.io/0.12/index.html) testing suite to run Jasmine tests on angularjs.

Once you have installed npm, you will need to install Karma globally. If you have installed Karma previously, you may skip this step.
>npm install -g karma-cli

From the root folder of the application, run:
> npm install

> npm test

This will find and run tests on the angular application.


### TODO:

- Update the query_pattern to make it more efficient at searching through smaller queries
- Cache the patterns to help autosuggestions in the search field