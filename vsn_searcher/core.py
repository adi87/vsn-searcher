import csv
from vsn_searcher import app
from flask import jsonify

from flask.ext.sqlalchemy import SQLAlchemy

db = SQLAlchemy(app)


class InvalidAPIUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        payload = dict(self.payload or ())
        rv = {
                'error': {
                    'message': self.message,
                    'status_code': self.status_code
                },
                'payload': payload
            }
        return rv


@app.errorhandler(InvalidAPIUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response
