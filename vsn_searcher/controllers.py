import os
from flask import render_template, jsonify, request, send_from_directory
from vsn_searcher import app
from vsn_searcher.core import db

from core import InvalidAPIUsage
from models import VsnSequence

from sqlalchemy.exc import IntegrityError

@app.route('/')
def home(**kwargs):
    """Routing for basic pages
    Pass routing onto the Angular app.
    """
    return render_template('index.html')


@app.route('/api/v1/vsnsequence', methods=['POST'])
@app.route('/api/v1/vsnsequence/<query>', methods=['GET'])
def vsn_sequence(query=None, **kwargs):
    """Method for adding and retrieving vsn sequences from the db
    Uses VsnSequence from models
    """
    if request.method == 'GET':
        if not VsnSequence.validate_sequence(query):
            raise InvalidAPIUsage("Invalid query", status_code=400)
        results = VsnSequence.query(query)
        return jsonify(matches=results)
    elif request.method == 'POST':
        sequence = request.get_json()
        try:
            new_sequence = VsnSequence(
                serial_number_pattern =  sequence.get('serial_number_pattern'),
                make =  sequence.get('make'),
                model =  sequence.get('model'),
                trim_name =  sequence.get('trim_name'),
                vehicle_trim_id =  sequence.get('vehicle_trim_id'),
                year =  sequence.get('year')
            )
        except ValueError as e:
            raise InvalidAPIUsage(str(e), status_code=400)
        try:
            db.session.add(new_sequence)
            db.session.commit()
        except IntegrityError as e:
            raise InvalidAPIUsage("A vsn_sequence exists with that ID", status_code=409)
        return jsonify(success=True)




# special file handlers and error handlers
@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'img/favicon.ico')


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404
