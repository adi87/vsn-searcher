import json
from vsn_searcher import app
from vsn_searcher.core import db
import re
import jellyfish

from sqlalchemy import text


def db_result_to_dict(results, cls):
    """
    Make a dict out of the sql alchemy query result.
    """
    convert = dict()
    res = []
    for r in results:
        d = dict()
        for i, c in enumerate(cls.__table__.columns):
            d[c.name] = r[i]
        res.append(d)
    return res


class VsnSequence(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    serial_number_pattern = db.Column(db.String(12))
    query_pattern = db.Column(db.String(12))
    make = db.Column(db.String(100))
    model = db.Column(db.String(100))
    trim_name = db.Column(db.Text)
    vehicle_trim_id = db.Column(db.String(10))
    year = db.Column(db.Integer)

    def __init__(self, serial_number_pattern, make, model, trim_name, vehicle_trim_id, year):
        """Initialize a VsnSequence with the appropriate parameters
        @params:
            serial_number_pattern: (str) The serial number pattern to search
            make: (str) the year the car was made
            model: (str) the model of the car
            trim_name: (str) the trim name for the car
            vehicle_trim_id: (str) the trim id for the car's trim
            year: (int) The year the car was made
        """
        self.serial_number_pattern = serial_number_pattern
        self.query_pattern = '%'+serial_number_pattern.replace('*','%') + '%'
        self.make = make
        self.model = model
        self.trim_name = trim_name
        self.vehicle_trim_id = vehicle_trim_id
        self.year = int(year)
        if not VsnSequence.validate_sequence(self.serial_number_pattern):
            raise ValueError("Invalid VSN Sequence")

    @staticmethod
    def validate_sequence(sequence):
        """Checks to see if the input sequence is valid
        Valid VSNs are in the format of six letters (A-Z) followed by six numbers (0-9)
        @params:
            sequence: (str) The sequence to validate

        return True if sequence is valid, False otherwise
        """
        pattern = '[A-Z*]{0,6}$' if len(sequence) <= 6 else '[A-Z*]{6,6}[0-9*]{0,6}$'
        res = re.match(pattern, sequence)
        match = res.group() if res else None
        if not match:
            return False
        else:
            return True

    @staticmethod
    def query(search_string):
        """Query the database for matches
        @params:
            search_string: (str) The string to query the DB for

        return the sorted resultSet from the db
        """
        sql = text("SELECT * FROM vsn_sequence"
            " WHERE '{}' LIKE query_pattern;".format(search_string))
        results = db.engine.execute(sql)
        results_dict = db_result_to_dict(results, VsnSequence)
        sorted_result = VsnSequence.sort_by_best_match(search_string, results_dict)
        return sorted_result

    @staticmethod
    def sort_by_best_match(query, matches):
        """Sort matches by the closest Levenshtein Distance

        @params:
            query: (str) the query string to get the distance for
            matches: (list) a list of matches with a key serial_number_pattern to match against

        return a sorted list
        """
        distances = []
        for i, m in enumerate(matches):
            vsn = m.get('serial_number_pattern')[:len(query)]
            distances.append(jellyfish.levenshtein_distance(vsn, query))
            m.update({'l_distance': distances[i]})
        return sorted(matches, key=lambda x: x['l_distance'])

    def __repr__(self):
        return '<VsnSequence {}>'.format(self.id)
