describe('Unit: Testing Controllers', function() {

    var $scope, ctrl, httpLocalBackend;
    beforeEach(angular.mock.module('VsnSearcher'));

    beforeEach(inject(function($httpBackend) {
        httpLocalBackend = $httpBackend;
    }));

    describe('SearchController', function () {

        beforeEach(inject(function($rootScope, $controller, $q, vsnSearch) {
            $scope = $rootScope.$new();

            ctrl = $controller('SearchController', {
                $scope: $scope
            });
        }));

        describe('testing performSearch successful search', function() {
            it('should make $scope.results an array of length 2', function() {
                var url = '/api/v1/vsnsequence/ZJTE';
                var mockSearchResults = {
                    matches: [{
                        serial_number_pattern: "ZJTE*V******"
                    }, {
                        serial_number_pattern: "ZJTE*V******"
                    }]
                };
                httpLocalBackend.expectGET(url).respond(200, mockSearchResults);
                $scope.performSearch("ZJTE");
                httpLocalBackend.flush();
                expect($scope.results.length).toEqual(2);
            });
        });

        describe('testing performSearch invalid search', function() {
            it('should make $scope.errorMessage not false', function() {
                var url = '/api/v1/vsnsequence/ZJ99TE';
                var httpResponse = {
                    error: {
                        message: "Invalid VSN Sequence",
                        status_code: 400
                    },
                    payload: {}
                };
                httpLocalBackend.expectGET(url).respond(400, httpResponse);
                $scope.performSearch("ZJ99TE");
                httpLocalBackend.flush();
                expect($scope.errorMessage).not.toBe(false);
            });
        });

        describe('testing $scope.$watch(formData.query)', function() {
            it('should return an errorMessage', function() {
                // this is an invalid code
                $scope.formData.query = 'ZA324A';
                $scope.$digest();
                expect($scope.errorMessage).not.toBe(false);
            });

            it('should return no error', function() {
                // this is a valid code
                $scope.formData.query = 'ZAAVCA';
                $scope.$digest();
                expect($scope.errorMessage).toEqual(false);
            });
        });
    });
});
