describe("Unit: Testing Services", function() {
    var searchFactory,
        http,
        invalidQuery = 'ZHJA88KJKSKJ',
        validQuery = 'ZHJAKJ324977';

    beforeEach(function() {
        angular.mock.module('vsnSearchServices');
    });

    beforeEach(inject(function(vsnSearch, $httpBackend) {
        searchFactory = vsnSearch;
        http = $httpBackend;
    }));

    describe("VSN Search Service:", function() {
        it('should contain a vsnSearch factory', function() {
            expect(searchFactory).toBeDefined();
        });
    });

    describe("vsnSearchServices validateQuery:", function() {
        it('should return false on invalid query', function() {
            expect(searchFactory.validateQuery(invalidQuery)).toEqual(false);
        });

        it('should return not false on valid query', function (){
            expect(searchFactory.validateQuery(validQuery)).not.toBe(false);
        });
    });

    describe('vsnSearchServices search', function (done) {
        it('should fetch search results', function() {
            var testResults = function (res) {
                expect(res.data.matches.length).toEqual(2);
            };
            var failTest = function (err) {
                expect(err).toBeUndefined();
            };
            var mockSearchResults = {
                    matches: [{
                        serial_number_pattern: "ZJTE*V******"
                    }, {
                        serial_number_pattern: "ZJTE*V******"
                    }]
                };
            http.expectGET('/api/v1/vsnsequence/'+validQuery).respond(200,mockSearchResults);
            searchFactory.search(validQuery)
                .then(testResults)
                .catch(failTest)
                .finally(done);

            http.flush();
        });
            
    });
});
