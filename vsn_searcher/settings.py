import os
current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.abspath(os.path.join(current_dir, os.pardir))


DEBUG = True
SECRET_KEY = 'temporary_secret_key'  # make sure to change this

DATA_PATH = '{}/data'.format(parent_dir)

SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/vsn_searcher.db'
