import os
import json
from flask import Flask

app = Flask(__name__)

app.config.from_object('vsn_searcher.settings')

app.url_map.strict_slashes = False

import vsn_searcher.core
import vsn_searcher.controllers
