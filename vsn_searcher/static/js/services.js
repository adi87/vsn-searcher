'use strict';

angular.module('vsnSearchServices', ['ngResource'])
    .factory('vsnSearch', ['$http', function($http) {
        return {
            validateQuery: function (query) {
                var pattern = (query.length <= 6) ? '^[A-Z]{0,6}$' : '^[A-Z]{6,}[0-9]{0,6}$',
                  re = new RegExp(pattern),
                  upperCaseQuery = query.toUpperCase(),
                  match = upperCaseQuery.match(pattern);
                return match || false;
            },
            search: function (query) {
                return $http.get('/api/v1/vsnsequence/'+query);
            }
        };
    }])
;



