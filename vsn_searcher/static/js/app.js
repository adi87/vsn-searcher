'use strict';

angular.module('VsnSearcher', ['vsnSearchServices', 'ui.router', 'ngRoute' ])
    .config(['$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise("/home");
            $stateProvider
                .state('home', {
                    abstract: true,
                    url: "/home",
                    templateUrl: "static/partials/home.html"
                })
                .state('home.search', {
                    url: "",
                    templateUrl: "static/partials/home.search.html",
                    controller: 'SearchController'
                })
            ;
        }
    ]).controller('BodyController', ['$scope', function ($scope) {
        $scope.windowHeight = $(window).height();
        $scope.windowWidth = $(window).width();
        $(window).on('resize', function() {
            $scope.$apply(function() {
                $scope.windowHeight = $(window).height();
                $scope.windowWidth = $(window).width();
            });
        });
    }]);
