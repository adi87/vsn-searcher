'use strict';

/* Controllers */

var module = angular.module('VsnSearcher');

/**
 * SearchController: Makes api calls using the vsnSearch service to fetch search results
 */
module.controller('SearchController', ['$scope', '$stateParams', '$timeout', 'vsnSearch', '$log', function($scope, $stateParams, $timeout, vsnSearch, $log) {
    var keyPressTimeout = null;
    $scope.query = $scope.queryText || null;
    $scope.errorMessage = false;
    $scope.results = [];
    $scope.formData = {};

    // make the actual API call here
    $scope.performSearch = function(query) {
        $scope.loading = true;
        vsnSearch.search(query)
            .then(function(res) {
                // display results in the view
                $scope.results = res.data.matches;
            }, function(err) {
                var message = '';
                // if there was an error fetching the data display the errorMessage in the view
                try {
                    message = err.data.error.message;
                } catch (e) {
                    message = err.statusText || "Unknown Exception";
                }
                showError(message);
                $log.error('Error fetching search results', err);
            });
    };


    $scope.onSubmit = function () {
        clearError();
        if($scope.searchForm.$valid){
            var query = $scope.formData.query;
            if(!vsnSearch.validateQuery(query)){
                showError('Invalid VSN Sequence');
                return;
            }
            $scope.performSearch(query);
        }
    };

    // watch the query for keystrokes and perform validation on the query
    $scope.$watch('formData.query', function(newVal, oldVal) {
        clearError();
        if (newVal) {
            $timeout.cancel(keyPressTimeout);
            //if the validateQuery service returns false, raise an error using errorMessage
            if(!vsnSearch.validateQuery(newVal)){
                showError('Invalid VSN Sequence');
                return;
            } else{
                // use a timeout so we we stagger API hits
                keyPressTimeout = $timeout(function () { 
                    $scope.performSearch(newVal);
                }, 300);
            }
        } else{
            $scope.results = [];
        }
    });


    function showError (message) {
        $scope.results = [];
        $scope.errorMessage = message;
    }

    function clearError () {
        $scope.errorMessage = false;
    }

}]);
