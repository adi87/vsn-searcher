import vsn_searcher
from flask.ext.testing import TestCase as Base, Twill


class TestCase(Base):

    def create_app(self):
        """Return a testing flask app."""
        return vsn_searcher.app


    def _test_get_request(self, endpoint, template=None, expected_status_code=200):
        response = self.client.get(endpoint)
        self.assert_status(response, expected_status_code)
        if template:
            self.assertTemplateUsed(name=template)
        return response


