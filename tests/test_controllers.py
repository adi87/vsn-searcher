
from tests import TestCase


class TestControllers(TestCase):

    def test_home(self):
        """Make sure home returns the correct template
        """
        self._test_get_request('/', template='index.html')


    def test_vsn_search(self):
        """Test the response for the search API
        The response should include a list of 'matches' if a valid string is passed.
        The response should throw a 400 if the input is invalid
        """
        valid_search = 'ZJTEAV000000'
        response = self._test_get_request('/api/v1/vsnsequence/{}'.format(valid_search))
        self.assertEquals(type(response.json.get('matches')), list)

        invalid_search = 'ZJT012AV'
        response = self._test_get_request('/api/v1/vsnsequence/{}'.format(invalid_search),
            expected_status_code=400)

    def test_page_not_found(self):
        """If the URL 404's make sure the template is accurate
        """
        response = self._test_get_request('/doesnotexist',
            template='404.html',
            expected_status_code=404)
