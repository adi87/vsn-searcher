import vsn_searcher

from vsn_searcher.models import VsnSequence
from tests import TestCase


class TestVsnSequence(TestCase):
    """Test the VsnSequence Model
    """

    def test_is_valid_sequence(self):
        """Tests to see if a ValueError is thrown on running validate_sequence
        Valid VSNs are in the format of six letters (A-Z) followed by six numbers (0-9)
        """
        valid_sequence = 'ZJTEAV000000'
        self.assertEqual(VsnSequence.validate_sequence(valid_sequence), True)

        invalid_sequence = 'ZJTEAV00AA'
        self.assertEqual(VsnSequence.validate_sequence(invalid_sequence), False)


    def test_query(self):
        """Tests to see if the find_in method of the VsnSequence class returns appropriate data
        """
        # find a match for a sequence
        test_sequence_returns_3 = 'XXRCAV000000'
        result = VsnSequence.query(test_sequence_returns_3)
        self.assertEqual(len(result), 3)

        # finds no match for a sequence
        test_sequence_returns_6 = 'ZJTEAV000000'
        result = VsnSequence.query(test_sequence_returns_6)
        self.assertEqual(len(result), 6)


    def test_best_match_sorting(self):
        """The find_in method should always return results sorted by best match
        """
        # unordered test data here
        test_data = [{
                "l_distance": 7,
                "make": "Volkswagen",
                "model": "GTI",
                "serial_number_pattern": "ZJTE*V******",
                "trim_name": "4-Door Autobahn, DSG",
                "vehicle_trim_id": "253929",
                "year": "2013"
            },
            {
                "l_distance": 1,
                "make": "Volkswagen",
                "model": "GTI",
                "serial_number_pattern": "ZJTE*V000000",
                "trim_name": "4-Door with Sunroof and Navigation, Manual",
                "vehicle_trim_id": "253923",
                "year": "2013"
            }]

        test_sequence = 'ZJTEAV000000'
        result = VsnSequence.sort_by_best_match(test_sequence, test_data)

        #the first result should be the one with more zero's in the serial_number_pattern
        self.assertEqual(result[0].get('serial_number_pattern'), 'ZJTE*V000000')

