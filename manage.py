import os
import json
import argparse
import requests
import csv

from vsn_searcher.core import db


def create_sample_db_entry(api_endpoint, payload):
    url = 'http://localhost:5000/' + api_endpoint
    r = requests.post(
        url, data=json.dumps(payload),
        headers={'Content-Type': 'application/json'})
    print r.text


def create_db():
    db.create_all()


def drop_db():
    db.drop_all()


def main():
    parser = argparse.ArgumentParser(
        description='Manage this application.')
    parser.add_argument(
        'command', help='the name of the command you want to run')
    parser.add_argument(
        '--seedfile', help='the file with data for seeding the database')
    args = parser.parse_args()

    if args.command == 'create_db':
        create_db()

        print "DB created!"
    elif args.command == 'delete_db':
        drop_db()

        print "DB deleted!"
    elif args.command == 'seed_db' and args.seedfile:
        items = []
        with open(args.seedfile) as csvfile:
            vsn_reader = csv.reader(csvfile)
            headers = vsn_reader.next()
            headers = [h.lower().replace(' ','_') for h in headers]
            for v in vsn_reader:
                items.append({headers[i]: k for i, k in enumerate(v)})

        for item in items:
            print item
            create_sample_db_entry('api/v1/vsnsequence', item)

        print "\nSample data added to database!"
    else:
        raise Exception('Invalid command')

if __name__ == '__main__':
    main()
